import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({});

  isLoading = false;

  constructor(private route: Router, private loginService: LoginService) {}

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('coffeecafe@gmail.com', [
        Validators.required,
        Validators.email,
      ]),
      password: new FormControl(46079, [Validators.required]),
    });
  }

  get email() {
    return this.loginForm.get('email');
  }
  get password() {
    return this.loginForm.get('password');
  }

  getErrorMessage() {
    if (this.email?.hasError('required')) {
      return 'You must enter email';
    }
    if (this.password?.hasError('required')) {
      return 'You must enter password';
    }

    return this.email?.hasError('email') ? 'Not a valid email' : '';
  }

  onLogin() {
    this.route.navigateByUrl('category');
    this.isLoading = true;
    let data = {
      Username: 'coffeecafe@gmail.com',
      password: 46079,
      AppVersion: 1.0,
      DeviceType: 1,
      DeviceID: 'c4db5cb7e4a46365',
      ModuleID: 1,
      FCMID:
        'ef5Dk7VIGsU:APA91bHhPJLrfv2oLhMCT8geh1v0sZGZyvtGXMvU_AAV6ncep3O_g1po0n1Wy_ACNm5HLv6KEjJgZ8I-SNJZnZ5wp1Bsz_QQi82YUTy8-0IwctLICHz_Zy49NBV0OowslifNcgrahad5',
    };

    this.loginService.login(data).subscribe(
      (res) => {
        console.log(res);
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
