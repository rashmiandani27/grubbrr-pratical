import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { SERVER_URL } from '../constant';
@Injectable({
  providedIn: 'root',
})
export class LoginService {
  window: any;
  db: any;
  constructor(private http: HttpClient) {}

  login(data: any) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')

      .set('Version', '1.0');

    return this.http
      .post(SERVER_URL + '/BranchLoginV2', data, { headers })
      .pipe(
        map(
          (res: any) => {
            return res;
          },
          (err: any) => {
            return err;
          }
        )
      );
  }
}
