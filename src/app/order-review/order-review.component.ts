import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { SERVER_URL } from '../constant';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-order-review',
  templateUrl: './order-review.component.html',
  styleUrls: ['./order-review.component.scss'],
})
export class OrderReviewComponent implements OnInit {
  url = SERVER_URL;
  odrders: any[] = [
    {
      CategoryID: 1,
      ItemID: 1,
      Name: 'R',
      ItemName: 'Item 1',
      ItemPrice: 1,
      Quantity: 1,
      ItemImageUrl: '',
      OrderTotal: 10,
    },
    {
      CategoryID: 1,
      ItemID: 1,
      Name: 'R',
      ItemName: 'Item 1',
      ItemPrice: 1,
      Quantity: 1,
      ItemImageUrl: '',
      OrderTotal: 10,
    },
  ];
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private loginService: LoginService
  ) {}

  ngOnInit(): void {
    this.loginService.window = this.document.defaultView;
    this.loginService.db = this.loginService.window.openDatabase(
      'mydb',
      '1.0',
      'My WebSql DB',
      2 * 1024 * 1024
    );
    this.getOrder();
  }
  async getOrder() {
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql('SELECT * FROM Orders', [], (tx: any, results: any) => {
        this.odrders = [...(results.rows as unknown as any[])];

        console.log('odrders results', this.odrders);
      });
    });
  }
  onAddQty(order: any) {
    order.Quantity++;
    this.incQty(order);
  }
  onRemoveQty(order: any) {
    order.Quantity--;
    this.decQty(order);
  }

  async incQty(item: any) {
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'UPDATE Orders SET Quantity = ' +
          item.Quantity +
          ' WHERE OrderID = ' +
          item.OrderID,
        [],
        (tx: any, results: any) => {
          if (results.rowsAffected) {
            console.log('order added');
          }
        }
      );
    });
  }

  async decQty(item: any) {
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'DELETE FROM Orders WHERE Orders.OrderID=' + item.OrderID,
        [],
        (tx: any, results: any) => {
          if (results.rowsAffected) {
            console.log('order removed');
          }
        }
      );
    });
  }
}
