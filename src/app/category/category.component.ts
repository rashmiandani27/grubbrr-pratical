import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { log } from 'console';
import { concat, Observable, of } from 'rxjs';
import { SERVER_URL } from '../constant';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent implements OnInit {
  url = SERVER_URL;
  currentCat: any = {
    CategoryID: 1,
    Name: 'R',
    Description: 'Afsfs',
    ImageUrl:
      '/uploads/categoryImage/99b1a6a5-73c9-4f83-b182-3e89dc67219b_images-removebg-preview.png',
  };
  catresults: any[] = [
    {
      CategoryID: 1,
      Name: 'R',
      Description: 'Afsfs',
      ImageUrl:
        '/uploads/categoryImage/99b1a6a5-73c9-4f83-b182-3e89dc67219b_images-removebg-preview.png',
    },
    {
      CategoryID: 2,
      Name: 'R',
      Description: 'Afsfs',
      ImageUrl:
        '/uploads/categoryImage/99b1a6a5-73c9-4f83-b182-3e89dc67219b_images-removebg-preview.png',
    },
  ];
  itemresults: any[] = [
    {
      ItemID: 1,
      Name: 'Item 1',
      FullDescription: 'Item Descroption 1',
      Price: 1,
      ImageUrl:
        '/uploads/categoryImage/99b1a6a5-73c9-4f83-b182-3e89dc67219b_images-removebg-preview.png',
      CategoryID: 1,
    },
    {
      ItemID: 1,
      Name: 'Item 1',
      FullDescription: 'Item Descroption 1',
      Price: 1,
      ImageUrl:
        '/uploads/categoryImage/99b1a6a5-73c9-4f83-b182-3e89dc67219b_images-removebg-preview.png',
      CategoryID: 1,
    },
    {
      ItemID: 1,
      Name: 'Item 1',
      FullDescription: 'Item Descroption 1',
      Price: 1,
      ImageUrl:
        '/uploads/categoryImage/99b1a6a5-73c9-4f83-b182-3e89dc67219b_images-removebg-preview.png',
      CategoryID: 1,
    },
    {
      ItemID: 1,
      Name: 'Item 1',
      FullDescription: 'Item Descroption 1',
      Price: 1,
      ImageUrl:
        '/uploads/categoryImage/99b1a6a5-73c9-4f83-b182-3e89dc67219b_images-removebg-preview.png',
      CategoryID: 1,
    },
    {
      ItemID: 1,
      Name: 'Item 1',
      FullDescription: 'Item Descroption 1',
      Price: 1,
      ImageUrl:
        '/uploads/categoryImage/99b1a6a5-73c9-4f83-b182-3e89dc67219b_images-removebg-preview.png',
      CategoryID: 1,
    },
    {
      ItemID: 1,
      Name: 'Item 1',
      FullDescription: 'Item Descroption 1',
      Price: 1,
      ImageUrl:
        '/uploads/categoryImage/99b1a6a5-73c9-4f83-b182-3e89dc67219b_images-removebg-preview.png',
      CategoryID: 1,
    },
    {
      ItemID: 1,
      Name: 'Item 1',
      FullDescription: 'Item Descroption 1',
      Price: 1,
      ImageUrl:
        '/uploads/categoryImage/99b1a6a5-73c9-4f83-b182-3e89dc67219b_images-removebg-preview.png',
      CategoryID: 1,
    },
    {
      ItemID: 1,
      Name: 'Item 1',
      FullDescription: 'Item Descroption 1',
      Price: 1,
      ImageUrl:
        '/uploads/categoryImage/99b1a6a5-73c9-4f83-b182-3e89dc67219b_images-removebg-preview.png',
      CategoryID: 1,
    },
    {
      ItemID: 1,
      Name: 'Item 1',
      FullDescription: 'Item Descroption 1',
      Price: 1,
      ImageUrl:
        '/uploads/categoryImage/99b1a6a5-73c9-4f83-b182-3e89dc67219b_images-removebg-preview.png',
      CategoryID: 1,
    },
  ];
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private route: Router,
    private loginService: LoginService
  ) {
    this.loginService.window = this.document.defaultView;
  }

  async ngOnInit(): Promise<void> {
    this.loginService.db = this.loginService.window.openDatabase(
      'mydb',
      '1.0',
      'My WebSql DB',
      2 * 1024 * 1024
    );

    await this.createTable();

    await this.getCat();
  }

  async createTable() {
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS CategoryMasters (CategoryID integer primary key, Name text, Description text)'
      );
    });
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS CategoryImages (CImgID integer primary key, ImageUrl text, CategoryID integer)'
      );
    });
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS Items (ItemID integer primary key, Name text, FullDescription text,Price float)'
      );
    });
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS ItemImages (IImgID integer primary key, ImageUrl text, ItemID integer)'
      );
    });
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS Item_Category_Mappings (CIMappingID integer primary key, CategoryID integer, ItemID integer)'
      );
    });
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS Orders (OrderID integer primary key, CategoryID integer, ItemID integer, CategoryName text, ItemName text, ItemImageUrl text, ItemPrice integer, Quantity integer, OrderTotal integer)'
      );
    });
  }

  async getCat() {
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'SELECT CategoryID,Name, Description, ImageUrl FROM CategoryMasters INNER JOIN CategoryImages ON CategoryMasters.CategoryID = CategoryImages.CategoryID',
        [],
        (tx: any, results: any) => {
          this.catresults = [...(results.rows as unknown as any[])];

          console.log('cat results', this.catresults);
        }
      );
    });
  }
  async getItem(cat: any) {
    this.currentCat = cat;
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'SELECT Items.ItemID Items.Name, Items.FullDescription, Items.Price, ItemImages.ImageUrl, Item_Category_Mappings.CategoryID FROM Items JOIN ItemImages ON Items.ItemID = ItemImages.ItemID JOIN Item_Category_Mappings ON Items.ItemID = Item_Category_Mappings.ItemID where Item_Category_Mappings.CategoryID = ' +
          cat.CategoryID,
        [],
        (tx: any, results: any) => {
          this.itemresults = [...(results.rows as unknown as any[])];

          console.log('item results', this.itemresults);
        }
      );
    });
  }

  async itemClick(item: any, qty: number) {
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'INSERT INTO Orders (CategoryID, ItemID,  CategoryName, ItemName, ItemImageUrl, ItemPrice, Quantity, OrderTotal) VALUES(?,?,?,?,?,?,?,?)',
        [
          this.currentCat.CategoryID,
          item.ItemID,
          this.currentCat.Name,
          item.Name,
          item.ImageUrl,
          item.Price,
          qty,
          item.Price * qty,
        ],
        (tx: any, results: any) => {
          if (results.rowsAffected) {
            console.log('order added to cart');
          }
        }
      );
    });
  }

  async insertCat(catname: any, catdesc: any) {
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'INSERT INTO CategoryMasters ( Name , Description ) VALUES(?,?)',
        [catname, catdesc],
        (tx: any, results: any) => {
          if (results.rowsAffected) {
            console.log('data inserted');
          }
        }
      );
    });
  }
  async insertCatImg(caturl: any, catId: any) {
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'INSERT INTO CategoryImages ( ImageUrl , CategoryID ) VALUES(?,?)',
        [caturl, catId],
        (tx: any, results: any) => {
          if (results.rowsAffected) {
            console.log('data inserted');
          }
        }
      );
    });
  }
  async insertItem(iname: any, idesc: any, iprice: any) {
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'INSERT INTO Items ( Name , FullDescription, Price) VALUES(?,?,?)',
        [iname, idesc, iprice],
        (tx: any, results: any) => {
          if (results.rowsAffected) {
            console.log('data inserted');
          }
        }
      );
    });
  }

  async insertItemImg(iurl: any, iId: any) {
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'INSERT INTO ItemImages ( ImageUrl , ItemID) VALUES(?,?)',
        [iurl, iId],
        (tx: any, results: any) => {
          if (results.rowsAffected) {
            console.log('data inserted');
          }
        }
      );
    });
  }
  async insertCatMap(catId: any, iId: any) {
    await this.loginService.db.transaction((tx: any) => {
      tx.executeSql(
        'INSERT INTO Item_Category_Mappings ( CategoryID , ItemID) VALUES(?,?)',
        [catId, iId],
        (tx: any, results: any) => {
          if (results.rowsAffected) {
            console.log('data inserted');
          }
        }
      );
    });
  }
  async addtoCart(data: object) {
    console.log('data', data);
  }

  onCheckout() {
    this.route.navigateByUrl('order');
  }
  onCancel() {
    this.route.navigateByUrl('login');
  }
}
